-module(devtest).
-export([(test/0)]).


simpleNetworkGraph () ->
  [{red  , [{white, [white, green]},
	    {blue , [blue]}]},
   {white, [{red, [blue]},
	    {blue, [green, red]}]},
   {blue , [{green, [white, green, red]}]},
   {green, [{red, [red, blue, white]}]}
  ].

test() ->
    Graph = simpleNetworkGraph(),
    RootPid = control:graphToNetwork(Graph),
    io:format("Root PID = ~w ~n.",[RootPid]),

    RootPid ! {message, green, self (), self (), []},
    receive 
        {trace, GreenPid, Trace} -> io:format ("Received trace: ~w~n", [Trace])
    end.
