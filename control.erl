%% @author Weinan WANG

-module(control).

%% ====================================================================
%% API functions
%% ====================================================================

-export([graphToNetwork/1,extendNetwork/4]).

graphToNetwork(Graph) ->
    NpMap = ets:new(name_pid_map,[]),
    RNameLst = [RName || {RName,_} <- Graph], 
    constructNetwork(RNameLst,NpMap),
    counteIncome(Graph,NpMap),
    sendInitMsg(Graph,NpMap),
    [RootRName|_] = RNameLst,
    [{_,{RootPid,_}}] = ets:lookup(NpMap,RootRName),
    RootPid.

extendNetwork (RootPid, SeqNo, ExecuteRouter, {NodeName, Edges}) -> 
    RouterCB = fun (RName,RTable) ->
                       [{_,Ref}] = ets:lookup(RTable,'$NoInEdges'),
                       if RName /= ExecuteRouter ->
                              Pair = ets:lookup(RTable,ExecuteRouter),
                              case Pair == [] of 
                                  true -> abort ;
                                  false -> 
                                      [{_,NodePid}] = Pair,
                                      EPidLst = [EPid || {EPid,_} <- Edges,EPid == self()],
                                      if EPidLst == [] -> [{NodeName,NodePid}];
                                         true ->[{NodeName,NodePid},{'$NoInEdges',Ref + 1}]
                                      end
                              end;
                          RName == ExecuteRouter ->
                              Pid = router:start(NodeName),
                              Pid ! {control,self(),self(),0,
                                     fun(_,Table) ->
                                             ets:insert(Table, {'$NoInEdges',1}),
                                             decodeEdges(Edges,Table)
                                     end},

                              io:format("Edges ~w,Rname ~w~n",[Edges,RName]),
                              EPidLst = [EPid || {EPid,_} <- Edges,EPid == self()],
                              %return Nodename,Pid pair when receive initdone
                              receive
                                  initdone -> 
                                      if EPidLst == [] -> [{NodeName,Pid}];
                                         true ->[{NodeName,Pid},{'$NoInEdges',Ref + 1}]
                                      end
                              end
                       end

               end,

    RootPid ! {control,self(),self(),SeqNo,RouterCB},
    receive
        {done,_,_} -> true;
        {rollbackdone,_,_} -> false
    end.
%% ====================================================================
%% Internal functions
%% ====================================================================

constructNetwork([],_)->[];
constructNetwork([RName|TRNameLst],NpMapId) ->
	Pid = router:start(RName),
    ets:insert(NpMapId,{RName,{Pid,0}}),
    constructNetwork(TRNameLst,NpMapId).

counteIncome([],_) -> true;
counteIncome([{_,Edges}|SubGraph],NpMap) ->
    EdgeNameLst = [EName || {EName,_} <- Edges],
	traverseEdges(EdgeNameLst,NpMap),
	counteIncome(SubGraph,NpMap).

traverseEdges([],_) -> true;
traverseEdges([EdgeName|TEdges],NpMap) ->	
	[{_,{CurPid,RefNo}}] = ets:lookup(NpMap, EdgeName),
 	RefNoNew = RefNo + 1,
 	ets:insert(NpMap, {EdgeName,{CurPid,RefNoNew}}),
	traverseEdges(TEdges, NpMap).


sendInitMsg([],_) -> null;
sendInitMsg([{RName,RouterLst}|TGraph],NpMap) ->
    [{_,{CurPid,RefNo}}] = ets:lookup(NpMap,RName),
    CurPid ! {control,self(),self(),0,
              fun(_,Table) ->
					  ets:insert(Table, {'$NoInEdges',RefNo}),
					  buildRouteringTab(RouterLst,NpMap,Table)
              end},

    receive
        initdone -> ok
    end,

    sendInitMsg(TGraph,NpMap).


buildRouteringTab([],_,_) -> true;
buildRouteringTab([{DestName,NameLst}|TRouterLst],NpMap,RTab) ->
	[{_,{DestPid,_}}] = ets:lookup(NpMap, DestName),
	populateRouteringTable(NameLst,DestPid,RTab),
	buildRouteringTab(TRouterLst,NpMap,RTab).

decodeEdges([],_) -> ok;
decodeEdges([{DestPid,NameLst}|TEdges],RTable) ->
    populateRouteringTable(NameLst,DestPid,RTable),
    decodeEdges(TEdges,RTable).

populateRouteringTable([],_,_) ->true;
populateRouteringTable([Name|TNameLst],DestPid,RTab) ->
	ets:insert(RTab, {Name,DestPid}),
	populateRouteringTable(TNameLst, DestPid, RTab).
