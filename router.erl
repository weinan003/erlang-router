-module(router).
-export([start/1]).

start(RouterName) ->
	spawn( fun()-> routerStart(RouterName) end).

routerStart(RouterName) ->
    Table = ets:new(routering,[private]),
    ControlTable = ets:new(controltab,[private]),
    receive
        {control,ControlPid,ControlPid,0,CBFunc} ->
            CBFunc(RouterName,Table),
            ControlPid ! initdone
    end,
    handler(RouterName,Table,ControlTable).

handler(RouterName,Table,ControlTable) ->
    Exit = fun () ->
                   ets:delete(ControlTable),
                   ets:delete(Table),
                   exit(self(),kill)
           end,
    receive
        {message,Dest,_,Pid,Trace} ->
            msgForwarding(RouterName,Table,Dest,Pid,Trace);
        {control,From,ControlPid,SeqNo,CBFunc} ->
            if From == ControlPid ->
                   Ret = CBFunc(RouterName,Table),

                   PidLst = sets:to_list(sets:from_list([Pid || {Name,Pid} <- ets:tab2list(Table),Name /= '$NoInEdges'])),
                   if Ret == abort -> 
                          lists:foreach(fun(P) ->
                                            P ! {rollback,self(),SeqNo}
                                    end,PidLst),
                          ets:insert(ControlTable,{SeqNo,rollback,PidLst,[],[],[],From});
                      true ->
                          lists:foreach(fun(P) ->
                                            P ! {control,self(),ControlPid,SeqNo,CBFunc}
                                    end,PidLst),
                          ets:insert(ControlTable,{SeqNo,Ret,PidLst,[],[],[],From}),
                          rootRouterMonitor(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc)
                   end;
               true->
                   ctrHandler(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc)
            end;
        {committed,Pid,SeqNo} ->
            Record = ets:lookup(ControlTable,SeqNo),
            if Record == [] ->
                   io:format("receive committed SeqNo = ~w,Pid = ~w,no record.~n",[SeqNo,Pid]);
               true ->
                   [{SeqNo,RetValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = Record,
                   if WaitingLst == [] ->
                          ok;
                      true ->
                          {NWaiting,NComm} = valueTranslate(Pid,WaitingLst,CommittedLst),
                          ets:insert(ControlTable,{SeqNo,RetValue,NWaiting,NComm,AbortLst,DupLst,XFrom}),
                          conditionalReply(SeqNo,ControlTable)
                   end
            end;
        {dup,Pid,SeqNo} ->
            Record = ets:lookup(ControlTable,SeqNo),
            if Record == [] ->
                   io:format("receive dup SeqNo = ~w,Pid = ~w,no record.~n",[SeqNo,Pid]);
               true ->
                   [{SeqNo,RetValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = Record,
                   if WaitingLst == [] ->
                          ok;
                      true ->
                          {NWaiting,NDup} = valueTranslate(Pid,WaitingLst,DupLst),
                          ets:insert(ControlTable,{SeqNo,RetValue,NWaiting,CommittedLst,AbortLst,NDup,XFrom}),
                          conditionalReply(SeqNo,ControlTable)
                   end
            end;
        {abort,Pid,SeqNo} ->
            Record = ets:lookup(ControlTable,SeqNo),
            io:format("~w in abort,Record = ~w~n",[RouterName,Record]),
            if Record == [] ->
                   io:format("receive abort SeqNo = ~w,Pid = ~w,no record.~n",[SeqNo,Pid]);
               true ->
                   [{SeqNo,RetValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = Record,
                   ets:insert(ControlTable,{SeqNo,RetValue,[],CommittedLst,lists:append(WaitingLst,AbortLst),DupLst,XFrom}),
                   conditionalReply(SeqNo,ControlTable)
            end;
        {rollback,From,SeqNo} ->
            rbHandler(ControlTable,Table,From,SeqNo);
        {submit,SeqNo} ->
            smHandler(ControlTable,Table,SeqNo);
        {done,From,SeqNo} ->
           [{SeqNo,ReturnValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = ets:lookup(ControlTable,SeqNo),
           NWaiting = lists:delete(From,WaitingLst),
           if NWaiting == [] -> XFrom ! {done,self(),SeqNo};
              true -> ets:insert(ControlTable,{SeqNo,ReturnValue,NWaiting,CommittedLst,AbortLst,DupLst,XFrom})
           end;
        {rollbackdone,From,SeqNo} ->
           [{SeqNo,ReturnValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = ets:lookup(ControlTable,SeqNo),
           NWaiting = lists:delete(From,WaitingLst),
           if NWaiting == [] -> XFrom ! {rollbackdone,self(),SeqNo};
              true -> ets:insert(ControlTable,{SeqNo,ReturnValue,NWaiting,CommittedLst,AbortLst,DupLst,XFrom})
           end;
        {dump,From} ->
            Dump = ets:match(Table,'$1'),
            From ! {table,self(),Dump};
        stop ->
            io:format("~w receive stop~n",[RouterName]),
            Exit()
    end,

    handler(RouterName,Table,ControlTable).

rootRouterMonitor(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc) ->
    receive
        {message,Dest,_,Pid,Trace} ->
            msgForwarding(RouterName,Table,Dest,Pid,Trace),
            rootRouterMonitor(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc);
        {control,XFrom,ControlPid,SeqNo,CBFunc} ->
            XFrom ! {dup,self(),SeqNo},
            rootRouterMonitor(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc);
        {committed,Pid,SeqNo}->
            [{SeqNo,RetValue,WaitingLst,CommittedLst,AbortLst,DupLst,XFrom}] = ets:lookup(ControlTable,SeqNo),
            {NWaiting,NComm} = valueTranslate(Pid,WaitingLst,CommittedLst),
            if NWaiting == [] ->
                   ets:insert(ControlTable,{SeqNo,submit,NComm,[],AbortLst,DupLst,XFrom}),
                   ets:insert(Table,RetValue),
                   lists:foreach(fun(P) ->
                                     P ! {submit,SeqNo}
                             end,NComm);
               true ->
                   ets:insert(ControlTable,{SeqNo,RetValue,NWaiting,NComm,AbortLst,DupLst,XFrom}),
                   rootRouterMonitor(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc)
            end;
        {abort,_,SeqNo} ->
            io:format("~w in root abort~n",[RouterName]),
            [{SeqNo,RetValue,WaitingLst,CommittedLst,AbortLst,_,_}] = ets:lookup(ControlTable,SeqNo),
            doRollback(Table,RetValue),
            PidLst = lists:append(WaitingLst,lists:append(CommittedLst,AbortLst)),
            lists:foreach(fun(P) ->
                              P ! {rollback,self(),SeqNo}
                      end,PidLst)
    after 5000 ->
              From ! {abort,self(),SeqNo},
              PidLst = sets:to_list(sets:from_list([Pid || {Name,Pid} <- ets:tab2list(Table),Name /= '$NoInEdges'])),
              lists:foreach(fun(P) ->
                                P ! {rollback,self(),SeqNo}
                        end,PidLst)
    end.

valueTranslate(P,Lst1,Lst2) ->
       NL1 = lists:delete(P,Lst1),
       NL2 = [P | Lst2],
       {NL1,NL2}.

conditionalReply(SeqNo,Table) ->
    [{SeqNo,_,WaitingLst,_,AbortLst,_,XFrom}] = ets:lookup(Table,SeqNo),
    if WaitingLst == [] ->
           if AbortLst /= [] ->
                  XFrom ! {abort,self(),SeqNo};
              true ->
                  XFrom ! {committed,self(),SeqNo}
           end;
           true -> ok
    end
    .

smHandler(ControlTable,Table,SeqNo) ->
    [{SeqNo,ReturnValue,_,CommittedLst,_,_,XFrom}] = ets:lookup(ControlTable,SeqNo),
    io:format("submit = ~w~n",[ReturnValue]),
    ets:insert(Table,ReturnValue),
    if CommittedLst == [] ->
           XFrom ! {done,self(),SeqNo};
       true ->
           lists:foreach(fun(P) ->
                                 P ! {submit,SeqNo}
                         end,CommittedLst)
    end,
    ets:insert(ControlTable,{SeqNo,submit,CommittedLst,[],[],[],XFrom}).

rbHandler(ControlTable,Table,From,SeqNo) ->
    Record = ets:lookup(ControlTable,SeqNo),
    if Record == [] ->
           PidLst = sets:to_list(sets:from_list([Pid || {Name,Pid} <- ets:tab2list(Table),Name /= '$NoInEdges'])),
           lists:foreach(fun(P) ->
                             P ! {rollback,self(),SeqNo}
                     end,PidLst),
           ets:insert(ControlTable,{SeqNo,rollback,PidLst,[],[],[],From});
       true ->
           [{SeqNo,ReturnValue,WaitingLst,CommittedLst,AbortLst,_,XFrom}] = Record,
           if XFrom == From ->
                  doRollback(Table,ReturnValue),
                  NWaiting = lists:append(lists:append(WaitingLst,CommittedLst),AbortLst),
                  lists:foreach(fun(P) ->
                                    P ! {rollback,self(),SeqNo}
                            end,
                            NWaiting),
                  ets:insert(ControlTable,{SeqNo,rollback,NWaiting,[],[],[],XFrom});
              true ->
                  From ! {rollbackdone,self(),SeqNo}
           end
    end.

ctrHandler(RouterName,Table,ControlTable,From,ControlPid,SeqNo,CBFunc) ->
    Record = ets:lookup(ControlTable,SeqNo),
    if Record == [] ->
           %dorealwork
           io:format("~w in control~n",[RouterName]),
           RetValue = CBFunc(RouterName,Table),
           PidLst = sets:to_list(sets:from_list([Pid || {Name,Pid} <- ets:tab2list(Table),Name /= '$NoInEdges'])),
           if RetValue == abort ->
                  From ! {abort,self(),SeqNo},
                  ets:insert(ControlTable,{SeqNo,RetValue,PidLst,[],[],PidLst,From});
              true ->
                  io:format("~w send control~n",[RouterName]),
                  lists:foreach(fun(P) ->
                                    P ! {control,self(),ControlPid,SeqNo,CBFunc}
                            end,PidLst),
                  ets:insert(ControlTable,{SeqNo,RetValue,PidLst,[],[],[],From})
           end;
       Record /= [] ->
           %break msg cycle
           From ! {dup,self(),SeqNo}
    end.

msgForwarding(RouterName,Table,Dest,Pid,Trace) ->
    NextTrace = [RouterName | Trace],
    if Dest /= RouterName ->
           DesInfo = ets:lookup(Table,Dest),
           if DesInfo /= [] ->
                  [{_,EdgePid}] = DesInfo,
                  EdgePid ! {message,Dest,self(),Pid,NextTrace};
              DesInfo == [] ->
                  io:format("receive a message to ~w,can not find router path ~n.",[Dest]),
                  Pid ! {trace,self(),lists:reverse(NextTrace)}
           end;
       Dest == RouterName ->
           FinalTrace = lists:reverse(NextTrace),
           Pid ! {trace,self(),FinalTrace}
    end.

doRollback(RTable,RetValue) -> 
    if RetValue == abort -> ok;
       true ->
           [{_,Pid}|_] = RetValue,
           Lst = lists:flatten(ets:match(RTable,{'$1',Pid})),
           if [] == Lst ->
                  Pid ! stop;
              true ->
                  ok
           end
    end.

